---
title: "Markdown standup date updating in Vim with vim-speeddating"
date: 2020-12-18T18:00:00
lastmod: 2020-12-18T18:00:00
draft: false
keywords: ["standdup", "vim", "vim-speeddating"]
description: "Updating dates in Markdown standup indexes using vim-speeddating in Vim"
tags: []
categories: ["technical", "vim", "standup"]
author: ""

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
postMetaInFooter: false
hiddenFromHomePage: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: false
reward: false
mathjax: false
mathjaxEnableSingleDollar: false
mathjaxEnableAutoNumber: false

# You unlisted posts you might want not want the header or footer to show
hideHeaderAndFooter: false

# You can enable or disable out-of-date content warning for individual post.
# Comment this out to use the global config.
#enableOutdatedInfoWarning: false

flowchartDiagrams:
  enable: false
  options: ""

sequenceDiagrams: 
  enable: false
  options: ""

---
# Standups

[Codethink](https://codethink.co.uk) runs daily [standup](https://www.codethink.co.uk/articles/2016/how-to-run-standups-on-ircslack/) meetings over [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) for each project to help keep on top of workload.

Each engineer writes a standup report in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, and pastes them in to the project's IRC channel when it's their turn. After the meeting finishes, that day's standup master collates all the reports and any discussion in a new page on the project's wiki on our Gitlab instance.

There's also an index page that needs to be kept up to date. If the project will be running for long enough, I'll usually add a week or two's worth of dates in advance every time I run out.

The format for each week is:

    # Week 52

    * [Fri 25th Dec](standups/ab123/20201225)
    * [Thu 24th Dec](standups/ab123/20201224)
    * [Wed 23rd Dec](standups/ab123/20201223)
    * [Tue 22nd Dec](standups/ab123/20201222)
    * [Mon 21st Dec](standups/ab123/20201221)

I'll copy and top paste the current week, and then edit accordingly. (Top pasting means that recent dates are the most easily accessible when the page loads.)

There's a minor downside to this in that unpopulated links are present. These will need removing if, for whatever reason, standup does not take place on a given day.

# The Dating Game

Editting the dates by hand is a phaff, so I was pleased to discover [Tim Pope](https://tpo.pe/) has written [vim-speeddating](https://github.com/tpope/vim-speeddating) (a plugin for my trext editor of choice, [Vim](https://www.vim.org/)). The clue's in the name, **vim-speeddating** speeds up manipulating dates in **Vim**. 

I can edit the wiki page in **Vim** through the [withExEditor](https://addons.mozilla.org/en-GB/firefox/addon/withexeditor) Firefox plugin, which makes text areas on webpages edittable in your choice of text editor (not just **Vim**).

With a little configuration, **vim-speeddating** allows me to select the lines I'd like to update, position the cursor over a digit of the day component of the date, type `7<Ctrl-A>` and be done. 

Kind of. This has to be done in two episodes, one for the dates inside the square brackets, and once more for those inside the round parentheses. But this is still a lot speedier and easier than hand crafting the changes.

# Ordinal Gotchas

There are a couple of things to look out for related to the ordinal day numbers - the ones with 'st', 'nd', 'rd', or 'th' suffixes (e.g. '1st', '26th', etc.). 

Firstly, after selecting a bunch of ordinal dates the cursor needs to be over the leading digit (e.g. the '1' of '18th'). For the eight digit dates the cursor can be over either of the final two digits (e.g. the '1' or '8' at the end of '20201218').

Secondly, ordinal dates can have one or two digits, and **vim-speeddating** doesn't handle this consistently when operating on a selection. So split weeks with mixed numbers of ordinal digits in to three episodes rather than the usual two. One for dates with a single ordinal digit, one for dates with two, and one for all of the eight digit dates.

The end result is:

    # Week 53

    * [ Fri 1st Jan](standups/ab123/20210101)
    * [Thu 31st Dec](standups/ab123/20201231)
    * [Wed 30th Dec](standups/ab123/20201230)
    * [Tue 29th Dec](standups/ab123/20201229)
    * [Mon 28th Dec](standups/ab123/20201228)

    # Week 52

    * [Fri 25th Dec](standups/ab123/20201225)
    * [Thu 24th Dec](standups/ab123/20201224)
    * [Wed 23rd Dec](standups/ab123/20201223)
    * [Tue 22nd Dec](standups/ab123/20201222)
    * [Mon 21st Dec](standups/ab123/20201221)

The leading space when ordinals rollover from two digits to one is slightly annoying, but of no real matter in this case since excess whitespace is elided in the final rendering of the eventual HTML output. [YMMV](https://www.urbandictionary.com/define.php?term=ymmv).

# Configuration

With the assumption that **vim-speeddating** has already been installed, the necessary one-time configuration consists of adding the following to **Vim**'s startup config file, after the point where the plugin has been loaded:

    au VimEnter * :SpeedDatingFormat %[2]0%0y%0m%0d 
    au VimEnter * :SpeedDatingFormat %a%[ ]%o%[ ]%b

I tried configuring a `SpeedDatingFormat`  to work in a single pass, but handling two dates at a time is beyond **vim-speeddating**'s remit and capabilities. Parsing dates is a real PITA, so I'm grateful to Tim Pope for writing the plugin at all!


