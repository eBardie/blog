---
title: "Microsoft VSCode and the Linux Kernel"
date: 2021-01-12T15:36:25Z
lastmod: 2021-01-12T15:36:25Z
draft: false
keywords: []
description: "Reminder of how to set up VSCode to display the Linux kernel's code"
tags: []
categories: ["Linux", "VSCode", "Editors"]
author: ""

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: false
autoCollapseToc: false
postMetaInFooter: false
hiddenFromHomePage: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: false
reward: false
mathjax: false
mathjaxEnableSingleDollar: false
mathjaxEnableAutoNumber: false

# You unlisted posts you might want not want the header or footer to show
hideHeaderAndFooter: false

# You can enable or disable out-of-date content warning for individual post.
# Comment this out to use the global config.
#enableOutdatedInfoWarning: false

flowchartDiagrams:
  enable: false
  options: ""

sequenceDiagrams: 
  enable: false
  options: ""

---

This is not something me from a year ago would've thought I'd be doing, let alone me from fifteen years ago (the last time I did any kernel work), but I'm using a Microsoft programme to work with the Linux kernel.
 
I've got a slew of VSCode extensions installed, most of them not specifically relevant to kernel work.

What are relevant are the extension:

    ms-vscode.cpptools (install in the usual way)

and this readymade set of .vscode config files along with a related script:

    https://github.com/amezin/vscode-linux-kernel (follow its instructions)


As [Marc.2377](https://stackoverflow.com/users/3258851/marc-2377) says [here](https://stackoverflow.com/questions/49198816/howto-use-the-vsc-to-navigate-linux-kernel-source#comment97314682_53843634):

    All defines are already in include/generated/autoconf.h (under /usr/src/linux-headers-$(uname -r)/), should be enough to forceInclude it in c_cpp_properties.json



<!--more-->
