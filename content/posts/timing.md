---
title: "On timing various methods of starting a secondary shell script"
date: 2022-04-07T21:24:00+01:00
lastmod: 2022-04-07T21:24:00+01:00
draft: false
keywords: ["bash","ksh", "shell", "time", "profiling", "linux"]
tags: ["timing"]
description: "Results of timing shell sourcing vs. shell execing"
categories: ["technical", "linux"]
author: "ebardie"

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
postMetaInFooter: false
hiddenFromHomePage: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: false
reward: false
mathjax: false
mathjaxEnableSingleDollar: false
mathjaxEnableAutoNumber: false

# You unlisted posts you might want not want the header or footer to show
hideHeaderAndFooter: false

# You can enable or disable out-of-date content warning for individual post.
# Comment this out to use the global config.
#enableOutdatedInfoWarning: false

flowchartDiagrams:
  enable: false
  options: ""

sequenceDiagrams: 
  enable: false
  options: ""

---

## Aims

I wanted to find out if there was a useful performance boost from sourcing a secondary shell script when compared with execing it in place.

I thought that the overhead of starting a second interpreter might be significant.

Another question I had was how much time is spent parsing the script compared with loading it?

And finally I've been told `bash` is less efficient than `ksh`, but what are the numbers?


## Method

I wrote a test script, `test.sh` that takes two parameters. 

{{< prismjs lang="shell" title="test.sh" file="/static/code/timing/test.sh" line-numbers="true" collapse="true" />}}

The first parameter is used to control the contents of the secondary script, i.e choosing which secondary script to run.

The second parameter controls which verb is used to run the secondary script. E.g. "source" or "exec"

The two secondary scripts both emit a message at both beginning and end. They also perform some trivial make work before the end. They differ in their main payload.

The file `secondary_comments.sh` has all of the above, but after the initial message there are 999 lines of comments, each consisting of a '#' followed by a ' ', followed by 103 '#'s, which pad the line to same length as the function lines in the next file.

{{< prismjs lang="shell" title="secondary_comments.sh" file="/static/code/timing/secondary_comments.sh" line-numbers="true" collapse="true" />}}

The file `secondary_functions.sh` has, after the initial message, 999 single-line functions, which are never called, each with the same trivial logic inside.

{{< prismjs lang="shell" title="secondary_functions.sh" file="/static/code/timing/secondary_functions.sh" line-numbers="true" collapse="true" />}}
 

### Notes

`/bin/time` was used rather than a shell builtin `time` command in order to control the output formatting.
`timeit` is a script I took from [StackOverflow](https://stackoverflow.com/a/57313672/2199078) that runs a given command a given number of times, and emits the average time taken to do so. It truncates trailing zeros, so I've massaged them back in to the data below.


## Results

The scripts were first run using `#!/bin/ksh`:

{{< details "raw output" >}}
    ❯ for payload in comments functions; do for verb in source exec ""; do /bin/time -f "\t%E real  (%U user,  %S sys)   %C\n" timeit 10000 ./test.sh ${payload} ${verb}; done; done
    0.0028180
            0:28.33 real  (17.47 user,  13.26 sys)   timeit 10000 ./test.sh comments source

    0.0027563
            0:27.82 real  (15.90 user,  13.52 sys)   timeit 10000 ./test.sh comments exec

    0.0042340
            0:42.90 real  (22.75 user,  22.45 sys)   timeit 10000 ./test.sh comments

    0.0085827
            1:25.88 real  (70.60 user,  16.87 sys)   timeit 10000 ./test.sh functions source

    0.0097378
            1:37.57 real  (77.75 user,  21.28 sys)   timeit 10000 ./test.sh functions exec

    0.0103031
            1:43.21 real  (86.92 user,  18.83 sys)   timeit 10000 ./test.sh functions
{{< /details >}}


The scripts were modified to run using `#!/bin/bash`:

{{< details "raw output" >}}
    ❯ for payload in comments functions; do for verb in source exec ""; do /bin/time -f "\t%E real  (%U user,  %S sys)   %C\n" timeit 10000 ./test.sh ${payload} ${verb}; done; done
    0.0062129
            1:01.52 real  (46.19 user,  19.42 sys)   timeit 10000 ./test.sh comments source

    0.0027639
            0:30.28 real  (17.36 user,  15.54 sys)   timeit 10000 ./test.sh comments exec

    0.0039244
            0:41.65 real  (25.84 user,  20.32 sys)   timeit 10000 ./test.sh comments

    0.0111439
            1:51.57 real  (91.43 user,  22.49 sys)   timeit 10000 ./test.sh functions source

    0.0118911
            1:59.01 real  (95.03 user,  26.28 sys)   timeit 10000 ./test.sh functions exec

    0.0124723
            2:05.06 real  (103.16 user,  25.11 sys)   timeit 10000 ./test.sh functions
{{< /details >}}


These are the results from a set of 10000 runs for each combination of payload * verb * shell:

|payload|verb|ksh|bash|
|---|---|---|---|
|comments|source|0.0028|0.0062|
|comments|exec|0.0028|0.0028|
|comments|run|0.0042|0.0039|
|functions|source|0.0089|0.0111|
|functions|exec|0.0097|0.0119|
|functions|run|0.0103|0.0125|

#### How variable are these values?

An obvious question is how variable are the results? Bear in mind these tests were performed on a laptop with a bunch of other software running, so you should expect some variability. I left for lunch after setting this off, and came back around where there's an uptick in the "function" data in the second hald of the eighth line, so that may have affected the course of these runs.

So, reconfiguring the `ksh` test so that each round of payload \* verb is output on a single line:

{{< details "raw output" >}}
    ❯ for n in {1..21}; do for payload in comments functions; do for verb in source exec ""; do timeit 10000 ./test.sh ${payload} ${verb} | tr '\n' ' '; done; done; echo; done
    0.0027080 0.0029444 0.0043179 0.0085136 0.0095973 0.0108439
    0.0022925 0.0023396 0.0040075 0.0083177 0.0094708 0.0106749
    0.0029824 0.0035239 0.0046004 0.0084181 0.0094095 0.0104619
    0.0031444 0.0035433 0.0049301 0.0076581 0.0085229 0.0102012
    0.0032091 0.0034327 0.0046659 0.0076088 0.0084775 0.0102398
    0.0029589 0.0039060 0.0047007 0.0074978 0.0084697 0.0104198
    0.0033391 0.0034405 0.0046683 0.0075024 0.0085012 0.0100112
    0.0035117 0.0032080 0.0045271 0.0080881 0.0090096 0.0103842
    0.0027649 0.0034274 0.0047552 0.0083319 0.0092300 0.0105594
    0.0033448 0.0037891 0.0046093 0.0083097 0.0093326 0.0106747
    0.0031976 0.0037164 0.0046780 0.0084776 0.0095837 0.0105860
    0.0029718 0.0032880 0.0048625 0.0083917 0.0094185 0.0104932
    0.0024963 0.0023878 0.0032779 0.0086185 0.0095094 0.0107272
    0.0031568 0.0027426 0.0042320 0.0084855 0.0095673 0.0107328
    0.0028229 0.0025728 0.0023713 0.0085387 0.0098490 0.0105563
    0.0029346 0.0029046 0.0036143 0.0085143 0.0095237 0.0106427
    0.0032318 0.0030693 0.0043342 0.0083086 0.0096010 0.0105081
    0.0026965 0.0029747 0.0046522 0.0085785 0.0098160 0.0105701
    0.0027677 0.0030047 0.0042994 0.0086136 0.0097156 0.0105160
    0.0029958 0.0036060 0.0038950 0.0086373 0.0094879 0.0105783
    0.0023866 0.0029289 0.0041860 0.0085043 0.0093781 0.0107134
{{< /details >}}

This shows some variability, but still broad distinction between columns.

    Averages across the twenty one ksh runs:
        
        Comments   Comments   Comments     Functions  Functions  Functions
        Source     Exec       Run          Source     Exec       Run
        0.0029     0.0032     0.0043       0.0083     0.0093     0.0105   

    Differences (from preceding column):
                   0.0005     0.0013                  0.0009     0.0011   

For completeness here's the same for `bash`:

{{< details "raw output" >}}
    ❯ for n in {1..100}; do for payload in comments functions; do for verb in source exec ""; do timeit 10000 ./test.sh ${payload} ${verb} | tr '\n' ' '; done; done; echo; done
    0.0056051 0.0027941 0.0032628 0.0118174 0.0124499 0.0128045
    0.0059279 0.0026708 0.0032888 0.0110267 0.0119784 0.0127087
    0.0054446 0.0026225 0.0032510 0.0112317 0.0129274 0.0134788
    0.0041255 0.0022078 0.0028771 0.0117044 0.0125180 0.0128556
    0.0050850 0.0023625 0.0032308 0.0118520 0.0131978 0.0128794
    0.0045246 0.0020477 0.0027685 0.0117677 0.0126714 0.0130033
    0.0050748 0.0020483 0.0031962 0.0118040 0.0123417 0.0131209
    0.0039956 0.0016915 0.0026434 0.0116337 0.0124976 0.0129042
    0.0045871 0.0019391 0.0031304 0.0116632 0.0124726 0.0131858
    0.0040183 0.0018726 0.0031132 0.0121144 0.0125456 0.0131050
    0.0040354 0.0020561 0.0032721 0.0116307 0.0127596 0.0130254
    0.0042555 0.0019484 0.0028448 0.0121810 0.0131473 0.0132172
    0.0045919 0.0024922 0.0029379 0.0120572 0.0132365 0.0132171
    0.0054593 0.0023580 0.0033981 0.0118563 0.0126058 0.0127683
    0.0050499 0.0024451 0.0031739 0.0114862 0.0119343 0.0129218
    0.0046086 0.0018861 0.0032752 0.0115351 0.0126783 0.0128334
    0.0043432 0.0019367 0.0028178 0.0116469 0.0122543 0.0126863
    0.0053838 0.0017659 0.0034031 0.0116852 0.0121220 0.0129826
    0.0062693 0.0027245 0.0037324 0.0117891 0.0122412 0.0133645
    0.0055143 0.0023908 0.0029357 0.0116227 0.0121135 0.0128300
    0.0046303 0.0024708 0.0022618 0.0117869 0.0120927 0.0127231
    0.0050756 0.0025812 0.0026551 0.0116071 0.0122337 0.0126045
    0.0055922 0.0026782 0.0036489 0.0112595 0.0120510 0.0125733
    0.0058448 0.0018847 0.0036227 0.0115994 0.0119602 0.0126841
    0.0060935 0.0024885 0.0038805 0.0114758 0.0124824 0.0128359
    0.0051500 0.0023636 0.0029890 0.0115410 0.0125189 0.0128347
    0.0055435 0.0024158 0.0032615 0.0114479 0.0125182 0.0129394
    0.0039256 0.0019058 0.0027722 0.0116349 0.0126925 0.0128259
    0.0047969 0.0024208 0.0037134 0.0115474 0.0124373 0.0128940
    0.0058633 0.0021174 0.0031569 0.0115753 0.0123179 0.0128212
    0.0050832 0.0018820 0.0030620 0.0117127 0.0120297 0.0126500
    0.0061315 0.0026335 0.0032531 0.0114510 0.0122406 0.0129100
    0.0064018 0.0026027 0.0032016 0.0116953 0.0123218 0.0125162
    0.0059251 0.0019813 0.0034181 0.0112565 0.0119307 0.0129191
    0.0046601 0.0023777 0.0034260 0.0114048 0.0120056 0.0126264
    0.0065733 0.0027958 0.0037631 0.0113292 0.0118265 0.0126611
    0.0058240 0.0022384 0.0023759 0.0118650 0.0123254 0.0128737
    0.0048732 0.0018366 0.0034218 0.0113749 0.0125874 0.0125432
    0.0051004 0.0025887 0.0034206 0.0113601 0.0121294 0.0126216
    0.0067298 0.0026226 0.0036317 0.0114766 0.0120858 0.0127365
{{< /details >}}

    Averages across the thirty nine bash runs:
        
        Comments   Comments   Comments     Functions  Functions  Functions
        Source     Exec       Run          Source     Exec       Run
        0.0052     0.0023     0.0032       0.0116     0.0124     0.0129

    Differences (from preceding column):
                  -0.0029     0.0009                  0.0008     0.0005

Values averaged across multiple runs:

|payload|verb|ksh (21)|bash (39)|
|---|---|---|---|
|comments|source|0.0029|0.0052|
|comments|exec|0.0032|0.0023|
|comments|run|0.0043|0.0032|
|functions|source|0.0083|0.0116|
|functions|exec|0.0093|0.0124|
|functions|run|0.0105|0.0129|

## Analysis

### comment lines vs. function lines

Reading in a line beginning with a comment is faster than reading in a line you can't just throw away after parsing the comment symbol. Again, everyone knew this.

What passes for "interesting" here are the figures for `bash`: why does sourcing take twice the time that execing does? With `ksh` the numbers are close enough to be within the margin of variablity.

### exec vs. fork

In place execing **is** faster than forking a new process. Who knew? 😉

### Which method is fastest?

For both `ksh` and `bash`, **sourcing** is most of a millisecond faster than execing. For `ksh` execing is just over a millisecond faster than simply running the secondary script, but for `bash` there's less of a difference.


### Which shell is fastest?

Clearly, when there's actual code to load, `ksh`'s results are all in the range of a millisecond or two faster than `bash`'s results.


## Conclusions

* `ksh` is faster than `bash` in these tests.

* Sourcing a secondary script file is consistently (a millisecond or two) faster than execing or running it.

So if milliseconds are important to you, and you must use a script, and your other constraints aren't indicating another choice, you might see some small benefit from using `ksh` and sourcing rather than execing or plain running your secondary scripts.


## Caveats

The test scripts aren't like real world scripts.  How `ksh` compares with `bash` when they're performing actual work may be radically different to these results. 

These results are true for these contrived scripts running on my laptop, for as long as they, today.

**Running** the script eventually returns control to the calling script, which in these tests continues with a little extra work when compared to the exec case which has **replaced** the calling script. This extra work will be causing a commensurate imbalance in the results. No, I don't plan to rerun these tests since sourcing also suffers from the same issue, but is still faster despite it.

*You* need to profile *your* scripts in *your* target environment.


## Coda. *Slight reprise.*

What is it with `bash` taking twice as long to source a script mainly composed of comment lines than it takes to exec it in place? Weird...
