---
title: "Echo cancellation, noise suppression, and all round sound improvement for Pulseaudio"
date: 2020-11-11T23:11:34Z
lastmod: 2020-11-11T23:11:34Z
draft: false
keywords: ["linux","audio","noise suppression","echo cancellation","pulseaudio","pamic","pulseeffects"]
description: "Fancy audio setup: sound sculpting, noise suppression, and echo cancellation"
tags: []
categories: ["technical", "linux", "audio"]
author: ""

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
postMetaInFooter: false
hiddenFromHomePage: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: false
reward: false
mathjax: false
mathjaxEnableSingleDollar: false
mathjaxEnableAutoNumber: false

# You unlisted posts you might want not want the header or footer to show
hideHeaderAndFooter: false

# You can enable or disable out-of-date content warning for individual post.
# Comment this out to use the global config.
#enableOutdatedInfoWarning: false

flowchartDiagrams:
  enable: false
  options: ""

sequenceDiagrams: 
  enable: false
  options: ""

---

I've sorted out three issues with sound on my work laptop, and as they might be of general interest, here, have a blog post. The laptop is running Debian Linux, and using Pulseaudio as the sound server.

## The problems

### Issue 1 : Audio Tweakage

I want the audio to sound good (to me). More bass, more detail/audio excitation, a wider stereo image, that sort of thing. 
  
My hearing isn't what it used to be (middle aged high-frequency notch) so I want to be able to tweak various audio qualities to account for my audiological foibles, and the idiosyncracies of the room I'm sitting in and/or the devices I'm using. 



### Issue 2 : Noise Suppression

My laptop's cooling capabilities are notoriously poorly designed, so the fan is often on, often loud, and always near enough to the built-in microphone to cause problems. When I first had the laptop and tried using the microphone on a conference call, someone said it sounded as though I was standing in front of a jet aircraft. I suspect that I had the microphone level up too high and there was some distortion involved as well, but fan noise certainly can be a problem. 

So if I can squash the fan noise, and ideally any other extraneous background noises, so much the better.


### Issue 3 : Echo Cancellation

In the days *Before*, I worked mainly at the [Codethink](https://www.codethink.co.uk/) offices, and not wanting to be a PITA to my colleagues, I used headphones when listening to music or videos, or when taking part in conference calls.
 
When working from home, and currently that's still every workday, my laptop's sound goes to an external amplifier and some half decent speakers. The sound is better than when using my headphones, and it's more comfortable for listening for extended durations.

The major downside is that the laptop's microphone picks up the sound coming from the speakers. This means that there can easily be a nasty echoey mess when on conference calls. 

Eliminating the echo whilst still being able to listen through the amplifier and speakers would be bonza.

# The Solutions

## Sound sculpting with PulseEffects

Having done some music recording on Linux using Jack, I am familiar with using various GUIs to set up audio processing pathways, flexibly chaining together different applications and modules to process or analyze the sound signal.

So I was delighted to discover that there's a Pulseaudio app called **PulseEffects** which can do something similar within Pulseaudio, albeit in a more constrained manner. **PulseEffects** inserts itself between sound emitting apps (e.g. Firefox when watching YouTube vids) and Pulseaudio's output, and has a configurable effects chain. 

This enables me to tweak the sound to account for the limitations of my hearing and the room I'm sitting in. It's currently configured to boost the bass, run some audio excitation, and fettle the stereo image.


## Noise suppression and Echo cancellation with pamic

When I looked into the possibility of noise suppression and/or echo cancellation in Pulseaudio, I was pointed to Pulseaudio's [module-echo-cancel](https://www.freedesktop.org/wiki/Software/PulseAudioDocumentation/User/Modules/#module-echo-cancel) and also [librnnoise_ladspa.so](https://github.com/werman/noise-suppression-for-voice) which can be co-opted in to use by **module-ladspa-sink** (Pulseaudio's generic LADSPA library wrapper module).

I found these tricky to configure, so I wrote [pamic](https://gitlab.com/eBardie/pamic) to help with that. *Caveat*: **librnnoise_ladspa.so** needs to be downloaded (or compiled) and installed manually, at least on Debian. I'll not repeat usage instructions here, since they're available at that Gitlab link already.

In addition to echo cancellation, **module-echo-cancel** can also handle noise suppression, just not quite as well as **librnnoise_ladspa.so**. It also uses more CPU overall. So in situations where echo cancellation isn't required (e.g. when using headphones) **pamic** will use **librnnoise_ladspa.so**. Huzzah!
 
**pamic** is already an improvement on handcrafting the relevant Pulseaudio config, but as it's a commandline tool you have to use it to list the available sources and sinks and then run it again with the relevant flags and values. So I'm thinking about revamping **pamic** as a GUI app to make it even easier to use: choose which mode you want to use, then use drop down lists to select sources and sinks as appropriate.

# The fly in the ointment 
## PulseEffects **and** pamic in echo cancellation mode

I've got audio tweakage capabilities from **PulseEffects**, and echo cancellation and/or noise suppression from **pamic**. How about both, simultaneously? Well, **pamic** sets itself up as the default output, so it should just work.

The gotcha here is that **pamic**'s **Echo-Cancel Sink Stream** *must* be blacklisted, otherwise Pulseaudio will detect a loop and there'll be only silence. 

1. In **PulseEffects**'s main tab...
2. select **Applications**...
3. find the **Echo-Cancel Sink Stream** item and...
4. click its **Blacklist** button.
5. Restart **PulseEffects**

(**pamic** and **PulseEffects** must both be running of course.) 

![Blacklist 'Echo-Cancel Sink Stream'](/img/pulseeffects_blacklist_echo_cancel_sink_stream.jpg)  

*[This image is of PulseEffects 4.8.0, using some GTK theme or other, and displayed in AwesomeWM. YMWV😄]*

(Bonus tip: If you need to *un*blacklist an app but can't see it in the list, go to the main hamburger menu, select the **Blacklist** tab, click on the speaker icon, and toggle **Show Blacklist Apps in Main Tab**. Again, restart **PulseEffects**.)
