---
title: "Profiling Python"
date: 2020-11-10T11:27:00+00:00
lastmod: 2020-11-10T11:27:00+00:00
draft: false
keywords: ["ffmpeg","webcam", "virtual webcam", "linux", "debian"]
tags: ["profiling","python"]
description: "Tools for profiling Python programmes"
categories: ["technical", "programming", "python"]
author: "ebardie"

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
postMetaInFooter: false
hiddenFromHomePage: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: false
reward: false
mathjax: false
mathjaxEnableSingleDollar: false
mathjaxEnableAutoNumber: false

# You unlisted posts you might want not want the header or footer to show
hideHeaderAndFooter: false

# You can enable or disable out-of-date content warning for individual post.
# Comment this out to use the global config.
#enableOutdatedInfoWarning: false

flowchartDiagrams:
  enable: false
  options: ""

sequenceDiagrams: 
  enable: false
  options: ""

---

I've written about profiling [topplot](https://gitlab.com/eBardie/topplot) over at the Codethink company [blog](https://www.codethink.co.uk/articles/2020/more-time-on-top-my-latest-work-improving-topplot/)(scroll down to the *Profiling* section).

There I talk about using the [statistical profiler](https://en.wikipedia.org/wiki/Profiling_(computer_programming)#Statistical_profilers) [py-spy](https://github.com/benfred/py-spy) to produce a flamegraph for a birdseye view of activity, and digging deeper with the [deterministic profiler](https://docs.python.org/3.9/library/profile.html#what-is-deterministic-profiling) [yappi](https://github.com/sumerc/yappi), graphing its output with [kcachegrind](https://github.com/KDE/kcachegrind).

Since then I've come across some more tools (notably through [Malek Cellier](https://github.com/malekcellier) in [this comment](https://github.com/microsoft/vscode-python/issues/10645#issuecomment-721698709)) and am noting them here for future reference. I intend to expand on what's here in quantity (if/when I come across other tools) and quality (when I get around to trying them out).

* [FunctionTrace](https://hacks.mozilla.org/2020/05/building-functiontrace-a-graphical-python-profiler/)
* [VizTracer](https://github.com/gaogaotiantian/viztracer)

Malek also mentioned in passing the [Tracy](https://github.com/wolfpld/tracy) C++ profiler, which *looks* v.cool.
