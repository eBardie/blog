#!/bin/bash

secondary="secondary_${1}.sh"
verb="${2}"

if [ "${verb}" == "source" ]; then 
    echo "sourcing ${secondary}"
    source ./"${secondary}"

elif [ "${verb}" == "exec" ]; then 
    echo "execing ${secondary}"
    exec ./"${secondary}"

elif [ "${verb}" == "" ]; then 
    echo "running ${secondary}"
    ./"${secondary}"

else
    echo "dunno the verb '${2}'"
    exit 1
fi

echo test.sh out
